//
//  MouseDragView.m
//  VADSIntegrated
//
//  Created by JiangZhiping on 16/4/7.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "MouseDragSelectableView.h"

@interface MouseDragSelectableView ()

@property (nonatomic) NSPoint startPoint;
@property (nonatomic) NSPoint endPoint;
@property (nonatomic, strong) CAShapeLayer *shapeLayer;

@end

@implementation MouseDragSelectableView


- (void)mouseDown:(NSEvent *)theEvent
{
    if (!self.touchEnabled) {
        return;
    }
    
    self.startPoint = [self convertPoint:theEvent.locationInWindow fromView:nil];
    
    // create and configure shape layer
    
    self.shapeLayer = [CAShapeLayer layer];
    self.shapeLayer.lineWidth = 1.0;
    self.shapeLayer.strokeColor = [NSColor greenColor].CGColor;
    self.shapeLayer.fillColor = [NSColor clearColor].CGColor;
    self.shapeLayer.lineDashPattern = @[@10, @5];
    [self.layer addSublayer:self.shapeLayer];
    
    // create animation for the layer
    
    CABasicAnimation *dashAnimation;
    dashAnimation = [CABasicAnimation animationWithKeyPath:@"lineDashPhase"];
    dashAnimation.fromValue = @0.0f;
    dashAnimation.toValue = @15.0f;
    dashAnimation.duration = 0.75f;
    [dashAnimation setRepeatCount:HUGE_VALF];
    [self.shapeLayer addAnimation:dashAnimation forKey:@"linePhase"];
}

- (void)mouseDragged:(NSEvent *)theEvent
{
    if (!self.touchEnabled) {
        return;
    }
    
    NSPoint point = [self convertPoint:theEvent.locationInWindow fromView:nil];
    
    // create path for the shape layer
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, self.startPoint.x, self.startPoint.y);
    CGPathAddLineToPoint(path, NULL, self.startPoint.x, point.y);
    CGPathAddLineToPoint(path, NULL, point.x, point.y);
    CGPathAddLineToPoint(path, NULL, point.x, self.startPoint.y);
    CGPathCloseSubpath(path);
    
    // set the shape layer's path
    
    self.shapeLayer.path = path;
    
    CGPathRelease(path);
}

- (void)mouseUp:(NSEvent *)theEvent
{
    if (!self.touchEnabled) {
        return;
    }
    // 给endpoint和startpoint加上约束，一定是在画面以内。
    _endPoint.x = MAX(_endPoint.x, 0);
    _endPoint.x = MIN(_endPoint.x, self.bounds.size.width);
    _endPoint.y = MAX(_endPoint.y, 0);
    _endPoint.y = MIN(_endPoint.y, self.bounds.size.height);

    self.endPoint = [self convertPoint:theEvent.locationInWindow fromView:nil];
    self.selectedRect = CGRectMake(MIN(_startPoint.x, _endPoint.x),
                                   MIN(_startPoint.y, _endPoint.y),
                                   fabs(_startPoint.x - _endPoint.x),
                                   fabs(_startPoint.y - _endPoint.y));
    [self.shapeLayer removeFromSuperlayer];
    self.shapeLayer = nil;
    self.selectedRect = [self convertRect:self.selectedRect toView:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MouseDragSelectionDone" object:[NSValue valueWithRect:self.selectedRect]];
}

@end
