//
//  MTKImageView.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/19.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "MTKImageView.h"

@interface MTKImageView ()

@property (nonatomic, strong) CIImage * ciimage;
@property (nonatomic, strong) CIContext *ciContext;
@property (nonatomic,strong) id<MTLCommandQueue> commandQueue;

@end

@implementation MTKImageView  {
    CGFloat screenBackingFactor;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self =[super initWithCoder:coder];
    if (self) {
        [self initJob];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frameRect device:(id<MTLDevice>)device {
    self = [super initWithFrame:frameRect device:device];
    if (self) {
        [self initJob];
    }
    return self;
}

- (instancetype) init {
    return [self initWithFrame:CGRectMake(0, 0, 1, 1) device:nil];
}

- (void) initJob {
    self.device = self.device ? self.device : MTLCreateSystemDefaultDevice();
    self.delegate = self;
    self.enableSetNeedsDisplay = YES;
    self.preferredFramesPerSecond = 0;
    self.paused = YES;
    self.framebufferOnly = NO; // This must be explicitly set to NO, otherwise the underlying metal texture is not rotated while device rotating.
    self.commandQueue = [self.device newCommandQueue];
    self.ciContext = [CIContext contextWithMTLDevice:self.device];
#if !TARGET_OS_IPHONE
    [self setWantsLayer:YES];
    super.autoresizingMask = NSViewHeightSizable | NSViewWidthSizable;
#else
    super.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
#endif
}

- (void)setImage:(id)image {
    _image = image;
    if (image) {
        CIImage * ciimage = [CIImage imageWithContent:_image];
        self.viewAspectRatio = ciimage.aspectRatio;
        ciimage = [ciimage resizeTo:CGSizeMake(self.window.screen.backingScaleFactor * self.frame.size.width , self.window.screen.backingScaleFactor * self.frame.size.height)];
        self.ciimage = ciimage;
#if TARGET_OS_IPHONE
        [self setNeedsDisplay];
#else
        self.needsDisplay = YES;
#endif
    }
}

- (void)drawInMTKView:(MTKView *)view {
    
#if !TARGET_OS_IPHONE
    if (self.inLiveResize) { // in case of weird crash.
        return;
    }
#endif
    
    if (!self.currentDrawable || self.drawableSize.width <= 0 || self.drawableSize.height <=0) {
        return;
    }
    
    id<MTLCommandBuffer> commandBuffer = [self.commandQueue commandBuffer];
    id<MTLTexture> outputTexture = self.currentDrawable.texture;
    [self.ciContext render:self.ciimage toMTLTexture:outputTexture commandBuffer:commandBuffer bounds:self.ciimage.extent colorSpace:CGColorSpaceCreateDeviceRGB()];
    [commandBuffer presentDrawable:self.currentDrawable];
    [commandBuffer commit];
}

- (void)mtkView:(MTKView *)view drawableSizeWillChange:(CGSize)size {
#if TARGET_OS_IPHONE
    [self setNeedsDisplay];
#else
    self.needsDisplay = YES;
#endif
}

@end
