//
//  FPSTimer.h
//  GazeEstimationOnCocoa
//
//  Created by JiangZhiping on 15/11/19.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TickTockTimer : NSObject

- (instancetype) initWithName:(NSString *)aName AverageLength:(NSInteger)anInteger NS_DESIGNATED_INITIALIZER;
- (void) tick;
- (void) tock;

@end
