//
//  MTKImageView.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/19.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <MetalKit/MetalKit.h>
#import <CocoaXtensions/CocoaXtensions.h>

@interface MTKImageView : MTKView<MTKViewDelegate>

/**
 the image to be shown in this view
 */
@property (nonatomic) id image;

- (instancetype) init;

@end
